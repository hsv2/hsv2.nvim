-- vi: et sw=4 ts=4:
-- hsv2
---@param branch string
---@param git string?
local function clone(branch, git)
    git = git or "git"
    if not vim.fn.executable(git) then
        return false, "missing git!"
    end
    local path = vim.fn.stdpath("data") .. "/site/pack/hsv2/opt/hsv2.nvim"
    if vim.fn.empty(vim.fn.glob(path)) > 0 then
        local output = vim.fn.system {
            git,
            "clone",
            "--branch",
            branch,
            "--depth=1",
            "https://git.disroot.org/soratobuneko/hsv2-neovim",
            path
        }
        return vim.v.shell_error == 0, output
    end
    return true
end

---Install hsv2.nvim plugin
---@param branch string
---|> "master"
---|  "dev"
---@param git    string? git executable
function HSV2_INSTALL(branch, git)
    local valid = { master = true, dev = true }
    branch = valid[branch] and branch or "master"
    local status, output = clone(branch, git)
    if not status then
        vim.api.nvim_echo({
            { "Failed to clone repository\n", "Error" },
            { output, "Error" },
        }, true, {})
        return
    end
    vim.api.nvim_echo({ { "Loading hsv2.nvim…" } }, true, {})
    vim.api.nvim_exec("packadd hsv2.nvim", false)
    local hsv2
    status, hsv2 = pcall(require, "hsv2")
    if status then
        vim.api.nvim_echo({ { "Loaded!" } }, true, {})
    else
        vim.api.nvim_echo({ { "Failed!\n", "Error" }, { hsv2, "Error" } }, true, {})
    end

    return hsv2
end
