-- vi: et sw=4 ts=4:
-- hsv2
---@module "hsv2"
---@class hsv2.hsv2_t: hsv2.object_t
local hsv2 = {}
---Init HSV2
---@generic config_t: table
---@param config config_t

local function on_event()
    
end

function hsv2:init(config)
    self._store = require("hsv2.prototypes.object"):new()
    ---@class hsv2.global_store: hsv2.object_t
    self._store = {
        config = require("hsv2.prototypes.config"):new(),
        enums  = require("hsv2.enums"):new(),
        log    = require("hsv2.log"):new(),
        uevent = require("hsv2.utils.uevent"):new(),
    }
    self._store.uevent:init(self._store)
    self._store.uevent:add_callback({ ".*" }, {
        callback = function (arg)
            vim.pretty_print("event:", arg)
        end
    })

    self._store.config:init(self._store)
    self._store.log:init(self._store)
    self._store.config:set(config)
    self._store = require("hsv2.utils.const")(self._store)
end

return require("hsv2.prototypes.object"):new(hsv2)
