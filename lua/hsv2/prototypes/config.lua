-- vi: et sw=4 ts=4
-- hsv2
---@class hsv2.config: hsv2.with_store_t, hsv2.object_t
local config = {}

---@class hsv2.config_t
config._config = {}

---@vararg string table key-path
function config:get(...)
    local value = vim.tbl_get(self._config or {}, ...)
    local function deep_map_const(value)
        if type(value) == "table" then
            value = vim.tbl_map(deep_map_const, value)
            return require("hsv2.utils.const")(value)
        end

        return value
    end

    return type(value) == "table" and vim.tbl_map(deep_map_const, value)
        or value
end

---@
function config:set(newconf, dont_exec_aucmds)
    vim.tbl_deep_extend("force", self._config or {}, newconf or {})
    if not dont_exec_aucmds then
        ---@type hsv2.uevent_utils_t
        local uev = self:store("uevent")
        uev:trigger("config_changed")
    end
end

---@generic store: hsv2.global_store
---@param store store
function config:init(store)
    self:_set_store(store)
end

require("hsv2.utils.multi_inherit")(config,
    require("hsv2.prototypes.with_store"))

return config
