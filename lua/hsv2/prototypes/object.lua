-- vi: et sw=4 ts=4:
-- hsv2
---@class hsv2.object_t
local object = {}

---New
---@generic T:   hsv2.object_t
---@generic U:   table
---@param   self T
---@param   t? U
---@return  T|U
function object:new(t, append_only)
    t = t or {}
    setmetatable(t, self)
    self.__index = self
    self.__pairs = self
    self.__ipairs = self
    if append_only then
        self.__newindex = function(_, k, v)
            if t[k] == nil then t[k] = v
            else error("tried to overwrite const t['" .. k .. "]", 2) end
            return t[k]
        end
    end

    return t
end

return object
