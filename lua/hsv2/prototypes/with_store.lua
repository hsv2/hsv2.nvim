-- vi: et sw=4 ts=4:
-- hsv2

---@class hsv2.with_store_t
local with_store = {}

---@generic store:    hsv2.object_t
---@param   store?    store
---@return  store|nil
function with_store:_set_store(store)
    self._store = store
    return self._store
end

---@param value any
---@param key   string
---@return any
function with_store:store_add(value, key)
    if self:store(key) == nil then
        self:store()[key] = value
        return value
    end
    error("cannot overwrite store[" .. key .. "]", 2)
end

---@generic store: hsv2.object_t
---@generic item:  hsv2.object_t
---@param   ...    string       table key path
---@return  item
function with_store:store(...)
    if ... == nil then
        return self._store
    end
    return vim.tbl_get(self:store(), ...)
end

return with_store
