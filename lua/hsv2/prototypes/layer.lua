-- vi: et sw=4 ts=4:
-- hsv2
---@class layer: hsv2.object_t
local layer = {}

function layer:load()
    assert(false, "implement me!")
end

function layer:on_config_update()
    assert(false, "implement me!")
end

function layer:unload()
    assert(false, "implement me!")
end

return require("hsv2.prototypes.object"):new(layer)
