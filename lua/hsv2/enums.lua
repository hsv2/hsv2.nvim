-- vi: et sw=4 ts=4:
-- hsv2
---@module "hsv2.enums"
---@class hsv2.enums: hsv2.object_t
local enums = {}

---@alias hsv2.uevent_t
---| "config-changed"
---| "key-doremap"
---@class hsv2.uevent.types
---@field config_changed string triggered on configuration change
---@field key_remap      string key mapping should be redone
enums.uevent = {
    ["config_changed"] = "HSV2ConfigChanged",
    ["key_doremap"] = "HSV2KeyRemap",
}

---@alias hsv2.log_level_t
---|  "all"     alias for the highest log level
---|  "debug"   Debug-level messages
---|  "info"    Informational
---|> "notice"  Normal but significant condition
---|  "warn"    Warning condition
---|  "err"     Error condition
---|  "crit"    Critical condition
---|  "alert"   Action must be taken immediately
---|  "emerg"   System is unusable
---@class hsv2.enums.log_level: table
---@field debug   integer Debug-level messages
---@field info    integer Informational
---@field notice  integer Normal but significant condition
---@field warn    integer Warning condition
---@field err     integer Error condition
---@field crit    integer Critical condition
---@field alert   integer Action must be taken immediately
---@field emerg   integer System is unusable
enums.log_level = {
    emerg  = 0,
    alert  = 1,
    crit   = 2,
    err    = 3,
    warn   = 4,
    notice = 5,
    info   = 6,
    debug  = 7,
}

require("hsv2.prototypes.object"):new(enums)

return enums
