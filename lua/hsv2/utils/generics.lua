-- vi: et sw=4 ts=4:
-- hsv2

---@module "hsv2.utils.generics"
local M = {}
local L = {}

---Ensure we get copies of reference types.
---@param  x    any
---@return any  x   `x` or deep copy of `x` if it's a table.
---@nodiscard
function M.ensure_copy(x)
    local copy = require("hsv2.utils.tables").copy

    return type(x) == "table" and copy(x)
        or type(x) == "function" and L.clone_function(x)
        or x
end

---Clone a function with its upvalues
---@param fn function
---@return function?
---@nodiscard
function L.clone_function(fn)
    local dumped = string.dump(fn)
    local cloned = loadstring(dumped)
    local i = 1
    while cloned and true do
        local name = debug.getupvalue(fn, i)
        if not name then
            break
        end
        debug.upvaluejoin(cloned, i, fn, i)
        i = i + 1
    end

    return cloned
end

return M
