-- vi: et sw=4 ts=4:
-- hsv2

---@module "hsv2.utils.asserts"
local M = {}

---Assert a given value is one of given types.
---@param  value    any         The value to check.
---@param  expected type|type[] Type name or list of type names.
---@param  name     string      Value name for error message.
---@param  opt?     boolean     Set the value as optional (allow nil).
---@return type     value_type  Value type.
function M.type(value, expected, name, opt)
    local tables = require("hsv2.utils.tables")

    expected = type(expected) == "string" and { expected }
        or expected
    local value_type = type(value)
    local caller = debug.getinfo(2, "n").name or ""
    assert(opt and value == nil or tables.one_of_values(value_type, expected),
        caller .. ": parameter '" .. name .. "' cannot be a " .. value_type)
    return value_type
end

return M
