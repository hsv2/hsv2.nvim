-- vi: et sw=4 ts=4
-- hsv2
---@class   hsv2.aucmd.cbck_opts_t
---@field   id                 integer
---@field   event              hsv2.uevent_t
---@field   group              string
---@field   buf                integer
---@field   file               string
--
---@alias   hsv2.aucmd.cbck_t  fun(opts: hsv2.aucmd.cbck_opts_t)
--
---@class   hsv2.uevent_utils_t: hsv2.with_store_t
---@field   _callbacks         table<hsv2.aucmd.cbck_t, integer>
---@field   new                fun(self: hsv2.uevent_utils_t, table):hsv2.uevent_utils_t
local uevent = {}
uevent._callbacks = {}

local L = {}

---@generic store: hsv2.global_store
---@param store store
function uevent:init(store)
    self:_set_store(store)
end

local default_augroup = "hsv2"
---@class hsv2.aucmd.opts_t
---@field callback hsv2.aucmd.cbck_t
---@field desc?    string
---@field group?   string
---@field nested?  boolean
---@field once?    boolean
--
---@param uev  hsv2.uevent_t|list<integer, hsv2.uevent_t>
---@param opts hsv2.aucmd.opts_t
function uevent:add_callback(uev, opts)
    opts.group = opts.group and "hsv2-" .. opts.group or default_augroup
    if L.valid_uev(self, uev) then
        self._callbacks[opts.group] = self._callbacks[opts.group] or {}
        self._callbacks[opts.group][uev] = self._callbacks[uev] or {}
        self._callbacks[opts.group][uev][opts.callback] = vim.api.nvim_create_autocmd("User", {
            callback = opts.callback,
            desc     = opts.desc,
            group    = opts.group,
            nested   = opts.nested,
            once     = opts.once,
            pattern  = uev,
        })
    end
end

function uevent:del_callback(uev, cb, group)
    local autocmd_id = vim.tbl_get(self._callbacks, group or default_augroup,
        uev, cb)
    if autocmd_id then
        vim.api.nvim_del_autocmd(autocmd_id)
    end
end

---
---@param uev   hsv2.uevent_t
---@param cb    hsv2.aucmd.cbck_t
---@param group string
function uevent:get_callbacks(uev, group)
    group = group or default_augroup
    local gid = vim.api.nvim_create_augroup(group, { clear = false })
    return vim.api.nvim_get_autocmds({
        event   = "User",
        group   = gid,
        pattern = L.valid_uev(self, uev),
    })
end

---@generic uev_t:    hsv2.uevent_t
---@param   uev       uev_t|list<integer, uev_t>
---@param   buffer?   integer        buffer ID
---@param   group?    string|integer
---|> "hsv2"
---@param   modeline? boolean        default: `true`
function uevent:trigger(uev, buffer, group, modeline)
    group = group and ("hsv2-" .. group) or "hsv2"
    local gid = vim.api.nvim_create_augroup(group, {
        clear = false,
    })
    vim.api.nvim_exec_autocmds("User", {
        buffer = buffer,
        group = gid,
        modeline = modeline,
        pattern = L.valid_uev(self, uev),
    })
end

---comment
---@generic uev_t: hsv2.uevent_t
---@param uevents uev_t|list<integer, uev_t>
---@return boolean|uev_t|list<integer, uev_t>
function L.valid_uev(self, uevents)
    ---@type string[]
    local valid_uevents = vim.tbl_keys(self:store("enums", "uevent"))
    local function valid(uev)
        for _, valid_uev in ipairs(valid_uevents) do
            if valid_uev:match(uev) then return true end
        end
        return false
    end

    for _, uev in ipairs(vim.tbl_islist(uevents) and uevents or { uevents }) do
        if not valid(uev) then
            error("Invalid user event '" .. uev .. "'", 2)
        end
    end

    return uevents
end

uevent = require("hsv2.utils.multi_inherit")(uevent,
    require("hsv2.prototypes.with_store"))

return uevent
