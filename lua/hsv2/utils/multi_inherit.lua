-- vi: et sw=4 ts=4:
-- hsv2
local priv = {}

---@generic C
---@generic D:   table
---@param  class C
---@param  ...   D[]
---@return C|D
local function compose_class(class, ...)
    local args = { ... }
    class = class or {}

    setmetatable(class, {
        __index = function(t, k)
            return priv.search_parents(k, args)
        end
    })
    class.__index = class

    function class:new(t)
        t = t or {}
        setmetatable(t, class)
        return t
    end

    return class
end

function priv.search_parents(key, parents)
    for _, parent in ipairs(parents) do
        local value = parent[key]
        if value ~= nil then return value end
    end
end

return compose_class
