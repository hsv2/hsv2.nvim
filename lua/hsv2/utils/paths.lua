-- vi: et sw=4 ts=4:
-- hsv2
---@module "hsv2.utils.paths"
local paths = {}

local str = require("hsv2.utils.str")
local asserts = require("hsv2.utils.asserts")

---@alias path_raw string|number
---@alias path     table<integer, path_raw>

---Parse an index number, dot separated keys into a list of keys.
---@param  raw_path? path_raw  Path component, or dot separated key names to parse.
---@return path?     path      Returns the path list of keys or nil if none.
---@nodiscard
function paths.parse(raw_path)
    asserts.type(raw_path, {
        "string",
        "number",
        "table",
    }, "path", true)

    local path = type(raw_path) == "string" and str.split(raw_path, "%.")
        or type(raw_path) == "number" and { raw_path }
        or type(raw_path) == "table" and raw_path or {}

    return next(path) and path or nil
end

---Returns string representation of a path.
---@param  path             path     Path to convert to string
---@param  prefix?          string
---@param  dont_parse_str?  boolean  if false (default) parse and unparse prefix
---@return string
---@nodiscard
function paths.to_string(path, prefix, dont_parse_str)
    asserts.type(path, {
        "string",
        "number",
        "table",
    }, "path")
    asserts.type(str, "string", "str", true)

    local parsed_path = paths.parse(path)
    local str = prefix or ""

    str = not dont_parse_str and paths.to_string(assert(paths.parse(str))) or str

    if not path then return str end

    for _, v in ipairs(parsed_path) do
        str = str .. (str ~= "" and "." or "")
        str = str .. v
    end

    return str
end

---Compare two path.
---@param  path1   path
---@param  path2   path
---@return boolean|number comp  `true`: paths are equals - `1`: 1st contains 2nd - `-1`: 2nd contains 1st - `false` paths are not related
---@nodiscard
function paths.compare(path1, path2)
    path1 = paths.parse(path1) or {}
    path2 = paths.parse(path2) or {}
    for i, key in ipairs(path1) do
        if path2[i] ~= key then
            return i > 1 and -1
        end
    end

    return #path2 > #path1 and 1 or true
end

return paths
