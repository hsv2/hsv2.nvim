-- vi: et sw=4 ts=4:
-- hsv2

---@module "hsv2.utils.str"
local M = {}

---@TODO: maybe multi chars delimiter
---Split string into a list of string using a delimiter pattern.
---@param  str          string      String to split.
---@param  delimiter    string      Delimter pattern.
---@param  keep?        boolean     Keep delimiter in results.
---@return table        str_list    List of strings.
---@nodiscard
function M.split(str, delimiter, keep)
    local asserts = require("hsv2.utils.asserts")
    asserts.type(str, "string", "str")
    asserts.type(delimiter, "string", "delimiter")
    keep = asserts.type(keep, "boolean", "keep", true) == "boolean" and keep

    ---@type string[]
    local str_list = {}
    local pattern = "([^" .. delimiter .. "]+)([" .. delimiter .. "]?)"
    for substr, delim in str:gmatch(pattern) do
        substr = keep and delim and substr .. delim or substr
        table.insert(str_list, substr)
    end

    return next(str_list) and str_list or nil
end

return M
