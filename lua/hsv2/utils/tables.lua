-- vi: et sw=4 ts=4:
-- hsv2
---@module "hsv2.utils.tables"
---@class hsv2.table_t: hsv2.object_t
local table_ext = {}
local L = {}

---For (".", true) returns true.
---For ("foo.bar.baz", { 1 }) returns { foo = { bar = { baz = { 1 } }} }.
---@param  path?        path
---@param  value        any
---@return table|any
---@nodiscard
function table_ext.nest_value(path, value)
    path = require("hsv2.utils.paths").parse(path)
    if not path then return value end
    return L.set({}, path, value)
end

---Check if t1 has a given key
---@param  t1  table
---@param  key string
---@return boolean
---@nodiscard
function table_ext.one_of_keys(key, t1)
    local res = vim.tbl_map(function(k)
        return k == key or nil
    end, vim.tbl_keys(t1))
    return res and next(res) ~= nil or false
end

---Check if value is one of values
---@param  value    any     Value to check.
---@param  expected any[]   A lists of possible values.
---@return boolean
---@nodiscard
function table_ext.one_of_values(value, expected)
    local res = vim.tbl_map(function(k)
        return k == value or nil
    end, vim.tbl_values(expected))
    return res and next(res) ~= nil or false
end

return table_ext
