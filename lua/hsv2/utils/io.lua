-- vi: et sw=4 ts=4:
-- hsv2
---@module "hsv2.utils.io"
local M = {}

---@alias hi_group string highlight group
---@alias echo_str { [1]: string, [2]?: hi_group } string and highlight group pair

---nvim_echo, basically
---@param msg              echo_str[]
---@param keep_in_history? boolean
function M.echo(msg, keep_in_history)
    keep_in_history = keep_in_history or false
    vim.api.nvim_echo(msg, keep_in_history, {})
end

return M
