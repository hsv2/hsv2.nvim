-- vi: et ts=4 sw=4:
-- hsv-2
---@module "hsv2.utils.log"
---@class hsv2.log: hsv2.with_store_t, hsv2.object_t
local log = {}
local L = {}

---@class hsv2.log.config @Logging configuration
---@field level hsv2.log_level_t
log._config = {
    level = "notice",
}

---Init log module
---@param store hsv2.global_store
function log:init(store)
    self._store = store
    assert(type(self._store.config) == "table")
    self._store.config:set({ log = vim.deepcopy(self._config) })
    self._store.uevent:add_callback("config_changed", {
        callback = L.get_conf_change_cb(self),
    })
end

---Log message at given log level
---@param msg    echo_str[]
---@param level? hsv2.log_level_t
function log:write(msg, level)
    msg = type(msg) == "string" and { { msg } } or msg
    local io = require("hsv2.utils.io")
    level = level or "notice"
    if L.level_cmp(self, level, log._config.level) <= 0 then
        local msg_with_prefix = vim.list_extend(L.msg_prefix(self, level), msg)
        io.echo(msg_with_prefix, true)
    end
end

---Compare log level priorities
---@param  l1      hsv2.log_level_t
---@param  l2      hsv2.log_level_t
---@return integer `positive`: l1 is greater - `zero`: l1 and l2 are equals - `negative` if l1 is lesser
---@nodiscard
function L.level_cmp(self, l1, l2)
    ---@type table<string, integer>
    local levels = self:store_get("enums", "log_level")
    local l1_id, l2_id = levels[L.level_get(self, l1)], levels[L.level_get(self, l2)]
    return l1_id - l2_id
end

---Takes a log level name or priority and return the level name if it exists or
---defaults to "notice".
---@param  level    hsv2.log_level_t|integer
---@return hsv2.log_level_t level `"notice"`: if not a valid log level - `"debug"` for "all" - `level` if param is a valid one
---@nodiscard
function L.level_get(self, level)
    local levels = self._store.enums.log_level
    if not level then return levels.notice end
    if level == "all" then return levels.debug end

    for name, prio in pairs(levels) do
        if level == name or level == prio then
            return name
        end
    end

    log.write({
        "invalid log level '",
        { tostring(level), "Keyword" },
        "' default to '",
        { "notice", "Keyword" },
        "'"
    }, "debug")

    return "notice"
end

---Get log message prefix string
---@param  level       hsv2.log_level_t
---@return echo_str[]  prefix
---@nodiscard
function L.msg_prefix(self, level)
    level = L.level_get(self, level)
    local prefix = {
        { "HSV2:", "Title" },
        { level },
        { ": " },
    }
    local level_hl = {
        err = "Error",
        warn = "Warning",
    }
    prefix[2][2] = level_hl[level] or "Normal"

    return prefix
end

---@param self hsv2.log
function L.get_conf_change_cb(self)
    return function()
        ---@type hsv2.log.config
        local store_conf = self:store("config"):get("log")
        if store_conf.level ~= vim.tbl_get(self._config, "log", "level")
            then
            vim.tbl_deep_extend("force", self._config.log, store_conf)
        end
    end
end

require("hsv2.utils.multi_inherit")(log,
    require("hsv2.prototypes.with_store"))

return log
