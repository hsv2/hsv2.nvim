-- vi: et sw=4 ts=4:
-- hsv2
---@class core_layer: layer
local core = {}
require("hsv2.prototypes.layer"):new(core)

local defaults = {
    leader = '<SPACE>',
    local_leader = ',',
}

function core:load()
    self:on_config_update()
end

function core:on_config_update()
    local g_conf = require("hsv2.prototypes.config"):get("core")
    local remap = false

    for _, k in ipairs({
        'leader',
        'local_leader',
    }) do
        if g_conf[k] ~= nil and g_conf[k] ~= vim.g[k] then
            vim.g[k] = g_conf[k]
            remap = true
        end
    end

    if remap then
        vim.api.nvim_exec_autocmds("User", {
                group = "hsv2",
                pattern = require("hsv2.events").key_remap,
            })
    end
end

function core:unload()
end

return core
