-- vi: et sw=4 ts=4:
-- hsv-2
local tests = {}

function tests.init_hsv2(config)
    tests.hsv2 = require("hsv2")
    vim.pretty_print(tests.hsv2, tests.hsv2:init(config))
end

return tests
