function _G.test(test, reload, ...)
    reload = reload == nil and true or reload
    if reload then
        package.loaded.tests = nil
        for module, _ in pairs(package.loaded) do
            if module:match("^hsv2") then
                vim.api.nvim_echo({ { "Unload module " .. module } }, true, {})
                package.loaded[module] = nil
            end
        end
    end
    require("tests")[test](...)
end
