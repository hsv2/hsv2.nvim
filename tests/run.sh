#!/bin/sh
# vi: et sw=4 ts=4:
# for macos
realpath() (
  OURPWD=$PWD
  cd "$(dirname "$1")"
  LINK=$(readlink "$(basename "$1")")
  while [ "$LINK" ]; do
    cd "$(dirname "$LINK")"
    LINK=$(readlink "$(basename "$1")")
  done
  REALPATH="$PWD/$(basename "$1")"
  cd "$OURPWD"
  echo "$REALPATH"
)

if [[ $TERMUX_VERSION ]];
then
  export TMPDIR="$TMUX_TMPDIR"
fi

export ROOT="$(dirname "$(dirname "$(realpath "$0")")")"
export TEST_TMP_DIR="$(mktemp --directory hsv2.nvim-test.XXXXXXXX)"
export XDG_DATA_HOME="$TEST_TMP_DIR/data"
export XDG_CONFIG_HOME="$TEST_TMP_DIR/conf"
export MYVIMRC="$ROOT/tests/init.lua"
export INSTALL_SCRIPT="$ROOT/install.lua"
export HSV2_LOG_LEVEL=all
export PLUG_DIR="$TEST_TMP_DIR/data/nvim/site/pack/hsv2/opt/hsv2.nvim"

mkdir -p "$PLUG_DIR"
ln -vs "$ROOT/lua" "$PLUG_DIR/"

echo data stored in $TEST_TMP_DIR
nvim -c "luafile $INSTALL_SCRIPT"\
     -c "lua hsv2 = HSV2_INSTALL('dev')" \
     -c "lua vim.opt.runtimepath:append('$ROOT/tests')" \
     -u "$MYVIMRC"
     $@;

rm -rf "$TEST_TMP_DIR"
