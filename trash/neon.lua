-- vi: et sw=4 ts=4:
-- hsv2
local M = {}

function M.config()
    local cmd = require("hsv2.utils.cmd")
    local c = require("hsv2.config")
    config = { hsv2 = c.hsv2(), plugin = c.plugin("neon") }
    if config.hsv2.theme == "neon" then
        if config.hsv2.fancy_font then
            for _, option in ipairs({
                -- TODO: move to defaults
                'bold',
                'italic_boolean',
                'italic_function',
                'italic_keyword',
                'italic_variable',
            }) do
                vim.g['neon_' .. option] = config.plugin[option]
            end
        end
        vim.api.nvim_exec('colorscheme neon', false)
    end
end

function M.packer_spec()
    return require("hsv2.utils.packer").make_spec('rafamadriz/neon', {
        config = M.config,
        cond = M.load_condition,
    })
end

function M.load_condition()
    return require("hsv2.config").plugin("neon").enabled == true
end

return M
