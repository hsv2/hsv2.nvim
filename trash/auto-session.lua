-- vi: et ts=4 sw=4:
-- hsv2
local M = {}
local _lua_module = 'auto-session'
local make_spec = require("hsv2.utils.packer").make_spec

function M.packer_spec()
    return make_spec('rmagatti/auto-session', {
        config = M.config,
        cond = M.load_condition(),
    })
end

function M.config()
    require("auto-session").setup(
        require("hsv2.config").plugin("auto-session").opts
    )
    for _, opt in ipairs({
        'blank',
        'buffers',
        'curdir',
        'folds',
        'help',
        'tabpages',
        'winsize',
        'winpos',
        'terminal',
    }) do
        vim.opt.sessionoptions:append(opt)
    end
end

function M.load_condition()
    return (require("hsv2.config")
        .plugin("auto-session").enabled == true)
end

return M
