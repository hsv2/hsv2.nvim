-- vi: et ts=4 sw=4:
-- hsv2
local M = {}

function M.packer_spec()
    return require("hsv2.utils.packer").make_spec('airblade/vim-rooter', {
        config = M.configure,
        cond = M.load_condition,
    })
end

function M.configure()
    require("hsv2.utils.cmd").set_vim_global('rooter_',
        require("hsv2.config").plugin("vim-rooter"))
end

function M.load_condition()
    return require("hsv2.config").plugin("vim-rooter").enabled
end

return M
