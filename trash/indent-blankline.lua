-- vi: et ts=4 sw=4
-- hsv2
local M = {}

function M.packer_spec()
    return require("hsv2.utils.packer").make_spec('lukas-reineke/indent-blankline.nvim', {
        requires = { 'nvim-treesitter/nvim-treesitter', opt = true },
        config = M.configure,
        cond = M.load_condition,
    })
end

M.plugin_name = "indent-blankline.nvim"

function M.configure()
    vim.opt.list = true
    vim.opt.listchars:append('space:⋅')
    vim.opt.listchars:append("eol:↴")
    require("indent_blankline").setup(require("hsv2.config")
        .plugin("indent-blankline").opts)
end

function M.load_condition()
    return (require("hsv2.config").plugin("indent-blankline")
        .enabled)
end

return M
