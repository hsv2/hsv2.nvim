-- vi: et sw=4 ts=4:
-- hsv2
local M = {}
local make_spec = require("hsv2.utils.packer").make_spec

function M.packer_spec()
    return make_spec('gentoo/gentoo-syntax', {
        cond = M.load_condition,
    })
end

function M.load_condition()
    return (require("hsv2.config")
        .plugin("gentoo-syntax").enabled)
end

return M
