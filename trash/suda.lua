-- vi: et ts=4 sw=4:
-- hsv2
local M = {}

function M.packer_spec()
    return require("hsv2.utils.packer").make_spec('lambdalisue/suda.vim', {
        config = M.configure,
        cond = M.load_condition,
    })
end

M.plugin_name = "suda.vim"

function M.configure()
    require('hsv2.utils.cmd').set_vim_global('suda',
        require("hsv2.config").plugin("suda").vim_globals)
end

function M.load_condition()
    return (require("hsv2.config").plugin("suda").enabled)
end

return M
