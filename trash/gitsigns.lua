-- vi: et sw=4 ts=4:
-- hsv2
local M = {}
local L = {}

function M.packer_spec()
    return require("hsv2.utils.packer").make_spec('lewis6991/gitsigns.nvim', {
        config = L.config,
        cond = L.load_condition,
    })
end

M.plugin_name = "gitsigns.nvim"

function L.config()
    local gs = require("gitsigns")
    gs.setup({
        current_line_blame = true,
        on_attach = function(bufnr)
            local map_keys = require("hsv2.setup").map_keys
            map_keys({
                    name = "Git",
                    opts = {
                        buffer = bufnr,
                    },
                    h = {
                        name = "Hunk"
                    },
                }, "<Leader>g")
            -- vim.keymap.set('n', '<Leader>ghn', PLUGINS.gitsigns.next_hunk, { buffer = bufnr, desc = 'Next' })
            -- vim.keymap.set('n', '<Leader>ghp', PLUGINS.gitsigns.prev_hunk, { buffer = bufnr, desc = 'Previous' })
            -- vim.keymap.set('n', '<Leader>ghh', PLUGINS.gitsigns.preview_hunk, { buffer = bufnr, desc = 'Preview' })
            -- vim.keymap.set({ 'n', 'v' }, '<Leader>ghs', PLUGINS.gitsigns.stage_hunk, { buffer = bufnr, desc = 'Stage' })
            -- vim.keymap.set('n', '<Leader>ghu', PLUGINS.gitsigns.undo_stage_hunk, { buffer = bufnr, desc = 'Unstage' })
            -- vim.keymap.set({ 'n', 'v' }, '<Leader>ghr', PLUGINS.gitsigns.reset_hunk, { buffer = bufnr, desc = 'Reset' })
        end,
    })
end

function L.load_condition()
    return require("hsv2.config").plugin("gitsigns").enabled == true
end

return M
