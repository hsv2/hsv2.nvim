-- vi: et ts=4 sw=4:
-- hsv2
local M = {}

function M.packer_spec()
    local make_spec = require("hsv2.utils.packer").make_spec
    return make_spec('nvim-lualine/lualine.nvim', {
        requires = { make_spec('kyazdani42/nvim-web-devicons') },
        config = M.configure,
        cond = M.load_condition,
    })
end

M.plugin_name = "lualine.nvim"

function M.configure()
    local config = require("hsv2.config")
    config = { hsv2 = config.hsv2(), plugin = config.plugin("lualine") }
    if config.plugin.opts.options.icons_enabled == nil then
        config.plugin.opts.options.icons_enabled = config.hsv2.fancy_font
    end
    require("lualine").setup(config.plugin.opts)
end

function M.load_condition()
    return require("hsv2.config").plugin("lualine").enabled
end

return M
