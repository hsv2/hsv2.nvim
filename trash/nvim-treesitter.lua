-- vi: et sw=4 ts=4:
-- hsv2
local M = {}

function M.packer_spec()
    return {
        'nvim-treesitter/nvim-treesitter',
        opt = true,
        config = M.configure,
        cond = M.load_condition,
    }
end

function M.configure()
    require('nvim-treesitter.configs').setup(
        require("hsv2.config").plugin("nvim-treesitter").opts
    )
end

function M.load_condition()
    return (require("hsv2.config").plugin("nvim-treesitter").enabled)
end

return M
