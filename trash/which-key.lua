-- vi: et sw=4 ts=4:
-- hsv2
local M = {}
local L = {}

function M.packer_spec()
    return require("hsv2.utils.packer").make_spec('folke/which-key.nvim', {
        config = L.config,
        cond = L.load_condition,
    })
end

M.plugin_name = "which-key.nvim"

function M.register_keymap(mappings, opts)
    if require("hsv2.utils.packer").has_plugin_loaded("which-key.nvim") then
        local cmd = require("hsv2.utils.cmd")
        local c = require("hsv2.config")
        local is_trace = c.log_level(c.config("enum").log_levels.trace)
        cmd.echo_info({
            "Register mapping to ",
            { "which-key", "Keyword" },
            "."
        })
        cmd.pprint({
            mappings = mappings,
            opts = opts,
        }, is_trace)
        L.plugin_api().register(mappings, opts)
    else
        require("hsv2.utils.cmd").echo_trace({
            "NOOP: ",
            { "which-key", "Keyword" },
            " not loaded."
        })
    end
end

function L.config()
    vim.opt.timeoutlen = 300
end

function L.load_condition()
    return require("hsv2.config").plugin("which-key").enabled == true
end

function L.plugin_api()
    return require("which-key")
end

return M
